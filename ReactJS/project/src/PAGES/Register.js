import React, { useRef, useState } from 'react'
import { Button } from 'reactstrap';
import axios from 'axios';
import { useNavigate, Link } from 'react-router-dom'


const Register = () => {


    const [email, setEmail] = useState('');
    const [mobile, setMobile] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [isValidEmail, setIsValidEmail] = useState(true);
    const [isValidMobile, setIsValidMobile] = useState(true);
    const [isValidPassword, setIsValidPassword] = useState(true);
    const [passwordsMatch, setPasswordsMatch] = useState(true);
    const username = useRef(null);
    const dateOfBirth = useRef(null);
  
    const handleEmailChange = (event) => {
      setEmail(event.target.value);
      setIsValidEmail(true); // Reset validation when input changes
    };
  
    const handleMobileChange = (event) => {
      setMobile(event.target.value);
      setIsValidMobile(true); // Reset validation when input changes
    };
  
    const handlePasswordChange = (event) => {
      setPassword(event.target.value);
      setIsValidPassword(true); // Reset validation when input changes
    };
  
    const handleConfirmPasswordChange = (event) => {
      setConfirmPassword(event.target.value);
      setPasswordsMatch(true); // Reset validation when input changes
    };
  
    const validateEmail = () => {
      const emailPattern = /^[a-zA-Z0-9]+@gmail\.com$/;
      setIsValidEmail(emailPattern.test(email));
    };
  
    const validateMobile = () => {
      const mobilePattern = /^\+91[6-9]\d{9}$/;
      setIsValidMobile(mobilePattern.test(mobile));
    };
  
    const validatePassword = () => {
      const passwordPattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{10,15}$/;
      setIsValidPassword(passwordPattern.test(password));
    };

        const [gender, setGender] = useState('');
      
        const handleGenderChange = (event) => {
          setGender(event.target.value);
        }
  
    const handleSubmit = async (event) => {
      event.preventDefault();
  
      // Validate email, mobile, and password fields
      validateEmail();
      validateMobile();
      validatePassword();
  
      // Check if passwords match
      if (password !== confirmPassword) {
        setPasswordsMatch(false);
        return;
      }
  
      try {
        // Send data to Express server using Axios
        const response = await axios.post('http://localhost:3005/register', {
          uname : username.current.value,
          dob: dateOfBirth.current.value,
          gen: gender,
          email: email,
          mob: mobile,
          pwd: password,
        });

        
  
        // Handle the response as needed
        console.log('Server response:', response.data);
      } catch (error) {
        // Handle errors
        console.error('Error sending data:', error);
      }
    };

    return (
        <form onSubmit={handleSubmit}>
                  <div>
        <input
          type="text"
          placeholder='User Name'
          ref={username}
          required
        />
      </div>
      <div>
        <input
          type="date"
          ref={dateOfBirth}
          required
        />
      </div>
      <div>
      <select value={gender} onChange={handleGenderChange}>
        <option value="">Gender</option>
        <option value="male">Male</option>
        <option value="female">Female</option>
        <option value="other">Other</option>
      </select>
      </div>
      <div>
        <input
          type="email"
          placeholder='example@gmail.com'
          value={email}
          onChange={handleEmailChange}
          onBlur={validateEmail}
          pattern="[a-zA-Z0-9]+@gmail\.com"
          title="Enter a valid Gmail address"
          required
        />
        {!isValidEmail && <p style={{ color: 'red' }}>Invalid email address.</p>}
      </div>

      <div>
        <input
          type="text"
          placeholder='+91 8456348989'
          value={mobile}
          onChange={handleMobileChange}
          onBlur={validateMobile}
          pattern="^\+91[6-9]\d{9}$"
          title="Enter a valid mobile number starting with +91"
          required
        />
        {!isValidMobile && <p style={{ color: 'red' }}>Invalid mobile number.</p>}
      </div>

      <div>
        <input
          type="password"
          placeholder='Password'
          value={password}
          onChange={handlePasswordChange}
          onBlur={validatePassword}
          pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{10,15}$"
          title="Enter a valid password"
          required
        />
        {!isValidPassword && (
          <p style={{ color: 'red' }}>
            Password must contain at least one uppercase letter, one lowercase letter, one number, and one special character. Minimum length: 10, Maximum length: 15.
          </p>
        )}
      </div>

      <div>
        <input
          type="password"
          placeholder='Password'
          value={confirmPassword}
          onChange={handleConfirmPasswordChange}
          required
        />
        {!passwordsMatch && <p style={{ color: 'red' }}>Passwords do not match.</p>}
      </div>

      <button type='submit'>Register</button>
    </form>
    )
}
export default Register;