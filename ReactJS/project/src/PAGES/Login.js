import React, {useState} from 'react'
import axios from 'axios';
import { Navigate, useNavigate } from 'react-router-dom';

const Login = () => {
  const [emailId, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [emailError, setEmailError] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [showPassword, setShowPassword] = useState(false);

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };
  const passwordInputType = showPassword ? 'text' : 'password';

  const validateEmail = () => {
    if (!emailId) {
      setEmailError('Email is required');
    } else if (!/\S+@\S+\.\S+/.test(emailId)) {
      setEmailError('Email is invalid');
    } else {
      setEmailError('');
    }
  };

  const validatePassword = () => {
    if (!password) {
      setPasswordError('Password is required');
    } else if (password.length < 6) {
      setPasswordError('Password must be at least 6 characters');
    } else {
      setPasswordError('');
    }
  };

  const handleLogin = async () => {
    try {
      const response = await axios.get(`http://localhost:3005/login/${emailId}/${password}`);
      if ( emailId === response.data.emailId && password === response.data.password) {
        console.log('Success');
      } 
      else {
        console.log('Invalid Credentials');
      }
    } catch (error) {
      console.log('Error:', error);
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    validateEmail();
    validatePassword();

    if (!emailError && !passwordError) {
      // Perform your form submission logic here.
    }
  };
        
  return (
    <div className="registration-container">
      <h2>Login</h2>
      <form onSubmit={handleSubmit}>
        

        <div className="form-group">
          <input
          placeholder='example@gmail.com'
            type="email"
            name="email"
              id="exampleEmail"
                value={emailId}
                onChange={(e) => setEmail(e.target.value)}
                onBlur={validateEmail}
            required
          />
        </div>

        <div className="form-group">
  
          <input
          placeholder='Password'
            type="password"
            required
            name="password"
            id="examplePassword"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            onBlur={validatePassword}
          />
        </div>

        <button type="submit" onClick={handleLogin}>Login</button>
      </form>
      <div className='reg'>
        <h4>Not a Member ? <a>Register</a></h4>
        </div>
    </div>
  )
}
 
export default Login;